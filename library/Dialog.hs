--------------------------------------------------------------------------------
-- |
-- module:    Dialog
-- copyright: (c) 2015 Nikita Churaev
-- license:   BSD3
--
-- === Basic usage example
--
-- > module Main where
-- > import Dialog
-- >
-- > main = dialog $ do
-- >   changeTitle "Hello, World!"
-- >   name <- askLine "What's your name?"
-- >   displayLine ("Your name is: " ++ name)
--------------------------------------------------------------------------------

module Dialog (
  -- * Running dialogs
  DialogT,
  DialogIO,
  dialog,

  -- * Changing title and end message
  changeTitle,
  changeEndMessage,

  -- * Simple messages
  displayLine,

  -- * Asking for input
  askLine,

  -- * Formatted messages
  display,
  p,
  pre,
  code,
  (<>),
  str,
  b,
  i,
  u,
  color,
  size,
  url,
  link,
  img,
  ol,
  ul,
  li,
  li',
  table,
  tr,
  th,
  td,
  th',
  td',
  columns,
  col,

  -- * Using IO
  MonadIO (..),

  -- * URLs
  URL (..),

  -- * Font size
  FontSize,

  -- * Colors
  Color,
  white,
  black,
  red,
  green,
  blue,
  rgb,
  toRGB
) where

--------------------------------------------------------------------------------

import Control.Monad.IO.Class (MonadIO (..))
import Data.Monoid ((<>))
import Dialog.Transformer
import Dialog.FormattedText
import Dialog.Shorthands
import Dialog.Run

--------------------------------------------------------------------------------
