--------------------------------------------------------------------------------
-- |
-- module:      Dialog.FormattedText
-- description: Formatted text data types
-- copyright:   (c) 2015 Nikita Churaev
-- license:     BSD3
--------------------------------------------------------------------------------

{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_HADDOCK not-home #-}

--------------------------------------------------------------------------------

module Dialog.FormattedText (
  Paragraph (..),
  FormattedText (..),
  HighlightLanguage (..),
  PictureSource (..),
  ListStyle (..),
  ListItem (..),
  Column (..),
  TableRow (..),
  TableCell (..),
  CellStyle (..),
  URL (..),
  FontSize,
  fontSizeValue,
  Color,
  white,
  black,
  red,
  green,
  blue,
  rgb,
  toRGB
) where

import Data.String (IsString (..))
import Data.Word (Word8)

--------------------------------------------------------------------------------

data Paragraph =
  TextParagraph FormattedText |
  Monospace (Maybe HighlightLanguage) String |
  Picture PictureSource |
  List ListStyle [ListItem] |
  Columns [Column] |
  Table [TableRow]
  deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

data FormattedText =
  Plain String |
  Colored Color FormattedText |
  Bold FormattedText |
  Italic FormattedText |
  Underline FormattedText |
  Size FontSize FormattedText |
  Link String FormattedText |
  CompositeText [FormattedText]
  deriving (Eq, Ord, Show)

instance IsString FormattedText where
  fromString string = Plain string

instance Monoid FormattedText where
  mempty = CompositeText []

  mappend (CompositeText []) text = 
    text
  mappend text (CompositeText []) = 
    text
  mappend (CompositeText a) (CompositeText b) =
    CompositeText (a ++ b)
  mappend a (CompositeText b) =
    CompositeText (a:b)
  mappend (CompositeText a) b =
    CompositeText (a ++ [b])
  mappend a b =
    CompositeText [a, b]

--------------------------------------------------------------------------------

newtype HighlightLanguage =
  HighlightLanguage String
  deriving (Eq, Ord, IsString, Show)

--------------------------------------------------------------------------------

data PictureSource
  = PictureFromURL URL
  deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

data ListStyle =
  NumberedList |
  BulletList
  deriving (Eq, Ord, Show)

newtype ListItem =
  ListItem [Paragraph]
  deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

newtype Column =
  Column [Paragraph]
  deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

newtype TableRow =
  TableRow [TableCell]
  deriving (Eq, Ord, Show)

data TableCell =
  TableCell CellStyle [Paragraph]
  deriving (Eq, Ord, Show)

data CellStyle =
  NormalCell |
  HeaderCell
  deriving (Eq, Ord, Show)

--------------------------------------------------------------------------------

-- | URL.
newtype URL =
  URL String
  deriving (Eq, Ord, IsString, Show)

--------------------------------------------------------------------------------

-- | Font size (in percent of the base font size).
newtype FontSize = FontSize {
    -- | Gets the value of a 'FontSize'.
    fontSizeValue :: Int
  } deriving (Eq, Ord)

instance Num FontSize where
  (FontSize a) + (FontSize b) = mkFontSize (a + b)
  (FontSize a) - (FontSize b) = mkFontSize (a - b)
  (FontSize a) * (FontSize b) = mkFontSize (a * b)
  negate = undefined "Can not negate a FontSize"
  signum _ = FontSize 1
  abs size = size
  fromInteger integer = mkFontSize (fromInteger integer)

instance Show FontSize where
  showsPrec prec (FontSize int) = showsPrec prec int

mkFontSize :: Int -> FontSize
mkFontSize int
  | int < 0 = 
      error ("FontSize " ++ show int ++ " is negative")
  | int == 0 = 
      error ("FontSize can not be zero")
  | int < 25 = 
      error ("FontSize " ++ show int ++ " is too small (maximum is 25)")
  | int > 1000 =
      error ("FontSize " ++ show int ++ " is too large (maximum is 1000)")
  | otherwise =
      FontSize int

--------------------------------------------------------------------------------

-- | Color.
data Color =
  Color Word8 Word8 Word8
  deriving (Eq, Ord, Show)

-- | Pure white (red: 255, green: 255, blue: 255).
white :: Color
white = Color 255 255 255

-- | Pure black (red: 0, green: 0, blue: 0).
black :: Color
black = Color 0 0 0

-- | Pure red (red: 255, green: 0, blue: 0).
red :: Color
red = Color 255 0 0

-- | Pure green (red: 0, green: 255, blue: 0).
green :: Color
green = Color 0 255 0

-- | Pure blue (red: 0, green: 0, blue: 255).
blue :: Color
blue = Color 0 0 255

-- | Makes a color from red, green and blue components.
rgb :: Word8 -> Word8 -> Word8 -> Color
rgb = Color

-- | Unpacks a color into red, green and blue components.
toRGB :: Color -> (Word8, Word8, Word8)
toRGB (Color r g b) = (r, g, b)

--------------------------------------------------------------------------------
