--------------------------------------------------------------------------------
-- |
-- module:    Dialog.JSON.ToJSON
-- copyright: (c) 2015 Nikita Churaev
-- license:   BSD3
--------------------------------------------------------------------------------

{-# LANGUAGE LambdaCase #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE ViewPatterns #-}
{-# LANGUAGE StandaloneDeriving #-}
{-# LANGUAGE GeneralizedNewtypeDeriving #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

--------------------------------------------------------------------------------

module Dialog.JSON.ToJSON () where

--------------------------------------------------------------------------------

import Data.Text (Text)
import Data.Aeson (ToJSON (..), (.=))
import Dialog.FormattedText
import qualified Data.Aeson as A
import qualified Data.Aeson.Types as AT

--------------------------------------------------------------------------------

typedObj :: Text -> [AT.Pair] -> A.Value
typedObj objType props = A.object (("type" .= objType) : props)

--------------------------------------------------------------------------------

instance ToJSON Paragraph where
  toJSON = \case
    TextParagraph text ->
      typedObj "text" [ "text" .= text ]
    Monospace language text ->
      typedObj "monospace" [ "language" .= language, "text" .= text ]
    Picture source ->
      typedObj "picture" [ "source" .= source ]
    List style items ->
      typedObj "list" [ "style" .= style, "items" .= items ]
    Table rows -> 
      typedObj "table" [ "rows" .= rows ]
    Columns columns ->
      typedObj "columns" [ "columns" .= columns ]

--------------------------------------------------------------------------------

deriving instance ToJSON HighlightLanguage

--------------------------------------------------------------------------------

deriving instance ToJSON ListItem

--------------------------------------------------------------------------------

deriving instance ToJSON TableRow

instance ToJSON TableCell where
  toJSON (TableCell style paragraphs) =
    A.object [ "style" .= style, "paragraphs" .= paragraphs ]

--------------------------------------------------------------------------------

deriving instance ToJSON Column

--------------------------------------------------------------------------------

instance ToJSON FormattedText where
  toJSON = \case
    Plain string ->
      typedObj "plain" [ "text" .= string ]
    Colored color text ->
      typedObj "colored" [ "color" .= color, "text" .= text ]
    Bold text ->
      typedObj "bold" [ "text" .= text ]
    Italic text ->
      typedObj "italic" [ "text" .= text ]
    Underline text ->
      typedObj "underline" [ "text" .= text ]
    Size size text ->
      typedObj "size" [ "size" .= size, "text" .= text ]
    Link url text ->
      typedObj "link" [ "url" .= url, "text" .= text ]
    CompositeText parts ->
      typedObj "composite" [ "parts" .= parts ]

--------------------------------------------------------------------------------

instance ToJSON PictureSource where
  toJSON (PictureFromURL url) = typedObj "url" [ "url" .= url ]

--------------------------------------------------------------------------------

instance ToJSON ListStyle where
  toJSON = \case
    NumberedList -> "numbered"
    BulletList   -> "bullet"

--------------------------------------------------------------------------------

instance ToJSON CellStyle where
  toJSON = \case
    NormalCell -> "normal"
    HeaderCell -> "header"

--------------------------------------------------------------------------------

deriving instance ToJSON URL

--------------------------------------------------------------------------------

instance ToJSON FontSize where
  toJSON fontSize = toJSON (fontSizeValue fontSize)

--------------------------------------------------------------------------------

instance ToJSON Color where
  toJSON (toRGB -> (r, g, b)) = typedObj "rgb" [ "r" .= r, "g" .= g, "b" .= b ]

--------------------------------------------------------------------------------
