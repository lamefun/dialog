--------------------------------------------------------------------------------
-- |
-- module:      Dialog.JSON.Utils
-- description: Formatted text data types
-- copyright:   (c) 2015 Nikita Churaev
-- license:     BSD3
--------------------------------------------------------------------------------

module Dialog.JSON.Utils (
  toJSONText
) where

import Data.Text (Text)
import Data.Aeson (ToJSON (toJSON))
import qualified Data.Text.Lazy as TL
import qualified Data.Text.Lazy.Builder as TLB
import qualified Data.Aeson.Encode as AE

--------------------------------------------------------------------------------

toJSONText :: ToJSON a => a -> Text
toJSONText value =
  TL.toStrict (TLB.toLazyText (AE.encodeToTextBuilder (toJSON value)))

--------------------------------------------------------------------------------
