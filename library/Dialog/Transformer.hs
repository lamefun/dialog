--------------------------------------------------------------------------------
-- |
-- module:      Dialog.Transformer
-- description: The Dialog monad transformer
-- copyright:   (c) 2015 Nikita Churaev
-- license:     BSD3
--------------------------------------------------------------------------------

{-# LANGUAGE GADTs #-}
{-# LANGUAGE RankNTypes #-}
{-# OPTIONS_HADDOCK not-home #-}

--------------------------------------------------------------------------------

module Dialog.Transformer where

--------------------------------------------------------------------------------

import Control.Monad (ap)
import Control.Monad.Trans.Class (MonadTrans (..))
import Control.Monad.IO.Class (MonadIO (..))
import Dialog.FormattedText

--------------------------------------------------------------------------------
-- Dialog monad transformer
--------------------------------------------------------------------------------

-- | Dialog monad transformer.
data DialogT m a where
  Pure :: a -> DialogT m a
  Bind ::
    (forall r. (forall b . DialogT m b -> (b -> DialogT m a) -> r) -> r) ->
    DialogT m a
  Lift :: m a -> DialogT m a
  ChangeTitle :: String -> DialogT m ()
  ChangeEndMessage :: String -> DialogT m ()
  Display :: [Paragraph] -> DialogT m ()
  AskLine :: String -> DialogT m String

instance Functor (DialogT m) where
  fmap func (Pure val) = Pure (func val)
  fmap func action = action >>= (\val -> pure (func val))

instance Applicative (DialogT m) where
  pure = Pure
  (<*>) = ap

instance Monad (DialogT m) where
  actionA >>= makeActionB = Bind (\func -> func actionA makeActionB)

instance MonadTrans DialogT where
  lift = Lift 

instance MonadIO m => MonadIO (DialogT m) where
  liftIO action = Lift (liftIO action)

-- | Dialog with 'IO' as the base monad.
type DialogIO = DialogT IO

--------------------------------------------------------------------------------
-- Changing dialog title
--------------------------------------------------------------------------------

-- | Changes the title of the dialog window. Default: @\"Dialog\"@
changeTitle :: String -> DialogT m ()
changeTitle = ChangeTitle

-- | Changes the end message of the dialog. Default: @\"End of program.\"@
changeEndMessage :: String -> DialogT m ()
changeEndMessage = ChangeEndMessage

--------------------------------------------------------------------------------
-- Displaying messages
--------------------------------------------------------------------------------

-- | Displays a message.
display :: [Paragraph] -> DialogT m ()
display = Display

--------------------------------------------------------------------------------
-- Asking for input
--------------------------------------------------------------------------------

-- | Asks the user for a line of text.
askLine :: String -> DialogT m String
askLine = AskLine

--------------------------------------------------------------------------------
