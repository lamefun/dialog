--------------------------------------------------------------------------------
-- |
-- module:      Dialog.Run
-- description: Running dialogs
-- copyright:   (c) 2015 Nikita Churaev
-- license:     BSD3
--------------------------------------------------------------------------------

{-# OPTIONS_HADDOCK not-home #-}

--------------------------------------------------------------------------------

module Dialog.Run (dialog) where

--------------------------------------------------------------------------------

import Control.Monad.IO.Class (MonadIO (..))
import Dialog.Transformer (DialogIO)
import Dialog.Run.WebkitGtk3 (runDialogUsingWebkitGtk3)

--------------------------------------------------------------------------------

-- | Opens a dialog window and runs the given dialog in it.
dialog :: MonadIO m => DialogIO () -> m ()
dialog = runDialogUsingWebkitGtk3

--------------------------------------------------------------------------------
