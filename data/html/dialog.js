//------------------------------------------------------------------------------

var ignoreEnter = true;

//------------------------------------------------------------------------------

function addMessage(msgClass, element) {
  var message =
    $("<div>", {class: "message " + msgClass})
      .append(element)
      .hide();
  $("#messages").append(message);
  $(message).find("pre code").each(function(i, block) {
    hljs.highlightBlock(block);
  });
  $(message).fadeIn(300);
  return message;
}

function dialogDisplay(paragraphs) {
  doThenScrollToEnd(function() {
    addMessage("display", createParagraphs(paragraphs))
  });
}

function dialogAskLine(prompt) {
  var input = $("<input>", {type: "text"});
  var layout = 
    $("<table>", {class: "prompt-layout"}).append(
      $("<tr>").append(
        $("<td>", {class: "prompt-message"}).append(prompt),
        $("<td>", {class: "prompt-entry ask-line"}).append(input)));

  var message = addMessage("prompt", layout);

  function pushResult() {
    $(input).prop("disabled", true);
    pushScriptOutput($(input).val());
    $(message).addClass("done");
  }

  $(input).keyup(function(e) {
    if (e.keyCode == 13 && !ignoreEnter) {
      pushResult();
    }
  });

  doThenScrollToEnd(function() {
    $(input).focus()
  });

  ignoreEnter = true;
  setTimeout(function() { ignoreEnter = false; }, 250);
}

function dialogEnd(message) {
  doThenScrollToEnd(function() {
    $("#end-dialog-message").text(message);
    $("#end-dialog").fadeIn(1000);
  });
}

function dialogException(message) {
  doThenScrollToEnd(function() {
    $("#end-dialog-message")
      .addClass("exception")
      .text("Exception: " + message);
    $("#end-dialog").fadeIn(600);
  });
}

function dialogReset() {
  $("#messages").empty();
  $("#end-dialog").removeClass("exception").hide();
}

function couldNotOpenBrowser(url) {
  alert("Could not open browser for URL: " + url);
}

//------------------------------------------------------------------------------

function doThenScrollToEnd(func) {
  var scrollHeight = $("#dialog")[0].scrollHeight;
  func();
  $("#dialog").prop("scrollHeight", scrollHeight);
  $("#dialog").animate({ scrollTop: $("#dialog")[0].scrollHeight }, 400);
}

//------------------------------------------------------------------------------

function pushScriptOutput(string) {
  $("#script-output")
    .empty()
    .append(document.createTextNode(string))
    .trigger("click");
}

//------------------------------------------------------------------------------

function createParagraphs(paragraphs) {
  return paragraphs.map(createParagraph);
}

//------------------------------------------------------------------------------

function createParagraph(paragraph) {
  return $("<p>").append(createParagraphContent(paragraph));
}

function createParagraphContent(paragraph) {
  var type = paragraph.type;
  if (type == "text") {
    return createFormattedText(paragraph.text);
  } else if (type == "monospace") {
    var codeClass = "nohighlight";
    if (paragraph.language) {
      if (hljs.getLanguage(paragraph.language)) {
        codeClass = paragraph.language;
      }
    }
    var code = scrollHack(
                 $("<pre>").append(
                   $("<code>", { class: codeClass }).text(paragraph.text)));
    return code;
  } else if (type == "picture") {
    return createPicture(paragraph.source);
  } else if (type == "list") {
    var tag = paragraph.style == "numbered" ? "<ol>"
            : paragraph.style == "bullet"   ? "<ul>"
            : undefined;
    return $(tag).append(paragraph.items.map(function(paragraphs) {
        return $("<li>").append(createParagraphs(paragraphs));
      }));
  } else if (type == "table") {
    var columnCount = 0;

    paragraph.rows.forEach(function(row) {
        if (columnCount < row.length) {
          columnCount = row.length;
        }
      });
  
    var trs = paragraph.rows.map(function(row) {
        return $("<tr>").append(row.map(function(cell) {
            return $("<td>", { class: cell.style })
                      .append(createParagraphs(cell.paragraphs));
          }));
      });

    return scrollHack(
             $("<div>", { class: "table-container" }).append(
               $("<table>", { class: "normal" }).append(
                 $("<tbody>").append(trs))));
  } else if (type == "columns") {
    var columns = paragraph.columns.map(function(column) {
        return $("<div>", { class: "column" }).append(createParagraphs(column));
      });
    return $("<div>", { class: "columns" }).append(columns);
  }
}

//------------------------------------------------------------------------------

function scrollHack(element) {
  return $("<table>", { class: "scroll-hack" }).append(
           $("<tbody>").append(
             $("<tr>").append(
               $("<td>").append(element))));
}

//------------------------------------------------------------------------------

function createFormattedText(text) {
  var type = text.type;
  if (type == "plain") {
    return document.createTextNode(text.text);
  } else if (type == "bold") {
    return $("<b>").append(createFormattedText(text.text));
  } else if (type == "italic") {
    return $("<i>").append(createFormattedText(text.text));
  } else if (type == "underline") {
    return $("<u>").append(createFormattedText(text.text));
  } else if (type == "colored") {
    var colorStr;
    var color = text.color;
    if (color.type == "rgb") {
      colorStr = "rgb(" + color.r + ", " + color.g + "," + color.b + ")";
    }
    return $("<span>", { style: "color: " + colorStr })
              .append(createFormattedText(text.text));
  } else if (type == "size") {
    var size = 10.5 * (text.size / 100);
    return $("<span>", { style: "font-size: " + size + "pt" })
              .append(createFormattedText(text.text));
  } else if (type == "link") {
    return $("<a>", { href: text.url }).append(createFormattedText(text.text));
  } else if (type == "composite") {
    return text.parts.map(createFormattedText);
  }
}

//------------------------------------------------------------------------------

function createPicture(source) {
  if (source.type == "url") {
    return $("<img>", { src: source.url });
  }
}

//------------------------------------------------------------------------------
