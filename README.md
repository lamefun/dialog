Dialog
======

[Hackage](https://hackage.haskell.org/package/dialog)

Dialog is a simple dialog-based UI library for Haskell. It aims to fulfill an
important role that have been traditionally occupied by the outdated and baroque
character terminal - providing Haskell beginners with a simple and easy to use
way to interact with their programs - 21st century style.

Basic demonstration
---------------------

![Basic example screenshot](screenshot-basic.png)

```haskell
import Data.Char (toLower)
import Dialog

main = dialog $ do
  name <- askLine "What's your name?"
  displayLine ("Hello, " ++ name ++ "!")
  askAboutLinux
  where
    askAboutLinux = do
      isGood <- askLine "Is Linux any good?"
      case map toLower isGood of
        "yes"   -> displayLine "Wrong answer! Shame on you!"
        "maybe" -> displayLine "You're only half-correct..."
        "no"    -> displayLine "This is indeed correct! Use Apple products!"
        _ -> do
          displayLine "Sorry, I didn't understand."
          askAboutLinux
```

```
$ stack ghc -- -threaded Main.hs
```

TODO
----

- Better documentation with more examples.
- Quasiquotes.
- Internationalization.
- More input options (multi-line text, file paths, radio buttons, checkboxes).
- Drawing, simple charts.
- Progress bars for time-consuming tasks.
- Text terminal backend.
- 100% Haskell GUI backend. (reflex-dom?)

Extended demonstration
----------------------

```haskell
{-# LANGUAGE OverloadedStrings #-}

module Main where

import Control.Monad (when)
import System.Directory (doesFileExist)
import Dialog

main = dialog $ do
  changeTitle "Hello, World!"
  changeEndMessage "Remeber, truth is good for you!"

  let
    superGood = color (rgb 50 120 0) . size 120 . b . u
    superBad  = color (rgb 200 50 0) . size 75

  display [
    p ("Truth is quite " <> b "true" <> " today! " <> "Let us talk " <>
       "about truth. Truth is what's true, falsehoods are all false " <>
       "without exception. Let me reiterate some well-known facts about " <>
       "the most fundamental concept of all which is commonly known as the " <>
       "concept of truth:"),
    ul [
      li "Truth has always been true.",
      li "Truth has never been, and will never ever be false."],
    p ("Use only " <> superGood "IMPERATIVE" <> " programming languages!"),
    p ("Avoid " <> superBad "\"functional\"" <> " programming languages!"),
    p ("Have a taste of true " <> b "IMPERATIVE" <> " programming: " <>
       url "http://tutorialspoint.com/csharp" <> "!"),
    table [
      tr [ th ""           , th "Games" , th "GUI"   , th "Mobile" ],
      tr [ th "functional" , td "Sucks" , td "Sucks" , td "Sucks"  ],
      tr [ th "IMPERATIVE" , td "Rocks" , td "Rocks" , td "Rocks"  ]],
    img "http://imgs.xkcd.com/comics/haskell.png"]

  name <- askLine "Enter your name"

  hasLinux <- liftIO (doesFileExist "/etc/lsb-release")
  displayLine (if hasLinux
                 then "It looks like you're using Linux, " ++ name ++ "... " ++
                      "Shame on you! Windows rocks!"
                 else "Uhm... nothing.")
```

![Extended example screenshot](screenshot-extended.png)
